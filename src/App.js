import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {setUsers, fetchUsers, setLoading, setError, setSelectedUser} from "./redux/actions";
import { Container } from "@material-ui/core";
import SelectUsers from "./components/SelectUsers";
import './App.css';
import { API_USERS_URI } from "./constants.json";

function App() {
  const dispatch = useDispatch();
  const loading = useSelector((state)=> state.usersManager.isLoading);
  const error = useSelector((state)=> state.usersManager.responseError);


  const User = ()=>{
      const selectedUser = useSelector((state)=> state.usersManager.selectedUser);

      return (
          <div>
              <p>{selectedUser?.name}</p>
          </div>
      )
  }

  useEffect(() => {
      dispatch(setLoading(true));
      fetchUsers(API_USERS_URI)
          .then((response) => response.json())
          .then((data)=>{
              dispatch(setLoading(false))
              dispatch(setUsers(data))
          })
          .catch((err)=>{
              dispatch(setError(err.message));
              dispatch(setLoading(false))
          })

    // fetch(API_USERS_URI)
    //     .then((response) => response.json())
    //     .then((data) => dispatch(setUsers(data)));
  }, [dispatch]);

  return (
    <Container>
        {loading?
            <p>LOADING</p>:
            <div>
                {error?
                    <p>{error}</p>:
                    <div>
                        <SelectUsers />
                        <User/>
                    </div>
                }
            </div>
        }







    </Container>
  );
}

export default App;
