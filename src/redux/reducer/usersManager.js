import {
    USERS_MANAGER_SET_USERS,
    USERS_MANAGER_SET_SELECTED_USER, USER_MANAGER_SET_LOADING, USER_MANAGER_SET_ERROR,
} from "../actions";

const initialState = {
    users: [],
    selectedUserId: "2",
    isLoading: false,
    responseError: null,
    selectedUser: {}
};



const usersManager = (state = initialState, { type, payload }) => {
    switch (type) {
        case USER_MANAGER_SET_LOADING:
            return {
                ...state,
                isLoading: payload,
            };

        case USER_MANAGER_SET_ERROR:
            return {
                ...state,
                responseError: payload,
            };

        case USERS_MANAGER_SET_USERS:
            return {
                ...state,
                users: payload
            };

        case USERS_MANAGER_SET_SELECTED_USER:
            const selectedUser = state.users.filter((user)=>user.id==payload)[0];
            return {
                ...state,
                selectedUserId: payload,
                selectedUser
            }

        default:
            return state;
    }
}

export default usersManager;
